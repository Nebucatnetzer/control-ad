function Restart-Domain-Computer
{
    try
    {
        $computername = Read-Host 'Please enter a computer name'
        Restart-Computer -Force -ComputerName $computername -ErrorAction Stop
        sleep 30
        while ($status -eq $null)
        {
            if(Test-Connection -Computername $computername `
              -BufferSize 16 `
              -Count 1 `
              -Quiet){break}
            Write-Host 'Still offline'
            sleep 5
        }
        Write-Host
        Write-Host -backgroundcolor "green" $computername ' is online'
        Write-Host
        Read-Host 'Press a key to quit'
    }
    catch
    {
        Write-Warning $_
        Read-Host 'Press a key to quit'
        break
    }
}

Restart-Domain-Computer
