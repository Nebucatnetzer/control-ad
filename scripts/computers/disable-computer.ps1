# load ActiveDirectory module
Try
{
    Import-Module ActiveDirectory -ErrorAction Stop
}
Catch
{
    Write-Warning $_
    Break
}

function Disable-Computer
{
    $samAccountName = Read-Host 'Please enter a computer name'
    $date = Get-Date -format "yyyy-MM-dd"
    Get-ADComputer -Identity $samAccountName | Disable-ADAccount
    Set-ADComputer -Identity $samAccountName `
      -Description "disabled by $(whoami) $date"
    Write-Host
    Write-Host $samAccountName ' has been disabled' -backgroundcolor green
    Write-Host
    Read-Host 'Press a key to continue'
}

Disable-Computer
