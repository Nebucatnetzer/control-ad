# load ActiveDirectory module
Try
{
    Import-Module ActiveDirectory -ErrorAction Stop
}
Catch
{
    Write-Warning $_
    Break
}

function Unlock-Account
{
    $username = Read-Host 'Please enter a user name'
    Try
    {
        Unlock-ADAccount -Identity $username -ErrorAction Stop
        Write-Host
        Write-Host $username ' unlocked' -backgroundcolor green
        Write-Host
    }
    Catch
    {
        Write-Warning $_
        Read-Host 'Press a key to continue'
        Break
    }
    Read-Host 'Press a key to continue'
}

Unlock-Account
