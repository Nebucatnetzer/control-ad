# load ActiveDirectory module
Try
{
    Import-Module ActiveDirectory -ErrorAction Stop
}
Catch
{
    Write-Warning $_
    Break
}

#Specify your default password here
$default_password = 'default-password'

function Reset-Password
{
    Try
    {
        $username = Read-Host 'Please enter a user name'
        Unlock-ADAccount -Identity $username
        Set-ADAccountPassword -Identity $username -Reset -NewPassword (
            ConvertTo-SecureString -AsPlainText $default_password -Force
        )
        Set-ADuser $username -ChangePasswordAtLogon $True
        Write-Host
        Write-Host 'Password of ' $username 'reset to ' $default_password `
          -backgroundcolor green
        Write-Host
        Read-Host 'Press a key to continue'
    }
    Catch
    {
        Write-Warning $_
        Read-Host 'Press a key to continue'
        Break
    }
}

Reset-Password
