function Remove-Public-Desktop-Icons
{
    $computername = Read-Host 'Enter the name of the computer'
    Remove-Item \\$computername\c$\users\public\desktop\*.lnk `
      -Exclude "SAP Logon Pad.lnk"
    Write-Host
    Write-Host 'Desktop icons removed' -backgroundcolor green
    Write-Host
    Read-Host 'Press a key to continue'
}

Remove-Public-Desktop-Icons
