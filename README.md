# control-ad

This is my collection of usefull powershell scripts. The idea is to
automate various tasks and provide other usefull functions.

### Usage
To just use the scripts on a day to day basis I recommend that you
just start the control-ad.ps1 script. This will then show you a simple
menu and let's you execute the various scripts from there.

The scripts however, are written to get started on their own as well
and can get converted into modules very easily. Each script contains a
function which then gets called on the last line. If you want to
convert the script into a module just delete the last line and rename
the script to script-name.psm1

Currently some functions might be a bit rough but I intend to clean
them up along the way.

### Todo

* Add a script to validate a user
  This way I could add a check to the scripts and inform the user that
  he used a wrong username
