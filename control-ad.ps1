# load ActiveDirectory module
Try
{
    Import-Module ActiveDirectory -ErrorAction Stop
}
Catch
{
    Write-Warning $_
    Break
}

function Show-Menu
{
    param (
        [string]$Title = 'Control Center'
    )
    cls
    Write-Host "================ $Title ================"

    Write-Host "1: Unlock User"
    Write-Host "2: Reset Password"
    Write-Host "3: Get Lockout Location"
    Write-Host "4: Disable Computer"
    Write-Host "5: Restart Computer"
    Write-Host "6: Remove Public Desktop Icons"
    Write-Host "Q: Press 'Q/q' to quit."
}

do
{
    Show-Menu
    $input = Read-Host "Please make a selection"
    switch ($input)
    {
        '1' {
            cls
            .\scripts\users\unlock-account.ps1
        } '2' {
            cls
            .\scripts\users\reset-password.ps1
        } '3' {
            cls
            start powershell `
              -ArgumentList '.\scripts\users\get-lockout-location.ps1'
        } '4' {
            cls
            .\scripts\computers\disable-computer.ps1
        } '5' {
            cls
            start powershell `
              -ArgumentList '.\scripts\computers\restart-computer.ps1'
        } '6' {
            cls
            .\scripts\misc\remove-public-desktop-icons.ps1
        } 'q' {
            return
        }
    }
}
until ($input -eq 'q')
